<?php

namespace Helium\EloquentInspectable\Traits;

use Helium\EloquentInspectable\Contracts\InspectableContract;

trait Inspectable
{
    protected function getInspectors(): array
    {
        return $this->inspectors ?? [];
    }

    public function inspect()
    {
        foreach ($this->getInspectors() as $inspector)
        {
            $inspector::inspect($this);
        }
    }

    public static function bootInspectable()
    {
        static::saving(function (InspectableContract $model) {
            $model->inspect();
        });
    }
}