<?php

namespace Helium\EloquentInspectable\Contracts;

interface InspectableContract
{
    public function inspect();
}