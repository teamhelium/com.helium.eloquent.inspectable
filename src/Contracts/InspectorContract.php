<?php

namespace Helium\EloquentInspectable\Contracts;

use Helium\EloquentInspectable\Traits\Inspectable;

abstract class InspectorContract
{
    abstract public static function inspect(Inspectable $inspectable);
}